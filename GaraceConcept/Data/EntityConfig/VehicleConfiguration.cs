﻿using GarageConcept.Models;
using System.Data.Entity.ModelConfiguration;

namespace GarageConcept.Data.EntityConfig
{
    public class VehicleConfiguration : EntityTypeConfiguration<Vehicle>
    {

        public VehicleConfiguration()
        {
            Property(o => o.PlateNumber).HasMaxLength(7);
            Property(o => o.ChassisNumber).HasMaxLength(20);

            HasIndex(e => new { e.PlateNumber }).IsUnique();
            HasIndex(e => new { e.ChassisNumber }).IsUnique();

        }

    }
}