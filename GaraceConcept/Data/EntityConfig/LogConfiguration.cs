﻿using GarageConcept.Data.Context;
using GarageConcept.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace GarageConcept.Data.EntityConfig
{
    public class LogConfiguration : EntityTypeConfiguration<Log>
    {

        public LogConfiguration()
        {
            Property(p => p.OldValue).HasMaxLength(4000);
            Property(p => p.NewValue).HasMaxLength(4000);
        }

    }
}