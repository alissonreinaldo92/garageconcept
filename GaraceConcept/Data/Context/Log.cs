﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GarageConcept.Data.Context
{
    public class Log
    {
        public string Id { get; set; }

        public string EntityName { get; set; }

        public string ObjectId { get; set; }

        public string Field { get; set; }

        public string OldValue { get; set; }

        public string NewValue { get; set; }

        public DateTime Register { get; set; }

        public string UserId { get; set; }

        public Log()
        {
            this.Id = Guid.NewGuid().ToString();
            this.Register = DateTime.Now;
        }

    }
}