﻿using GarageConcept.Data.EntityConfig;
using GarageConcept.Migrations;
using GarageConcept.Models;
using GarageConcept.Models.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace GarageConcept.Data.Context
{
    public class GarageConceptContext : IdentityDbContext<User>
    {

        public DbSet<Log> Logs { get; set; }

        public DbSet<Vehicle> Vehicles { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderHistory> OrdersHistory { get; set; }

        public DbSet<OrderImage> OrderImages { get; set; }

        private string CreatorUserId { get; set; }

        private readonly Dictionary<int, Dictionary<string, object>> ValuesBeforeChange = new Dictionary<int, Dictionary<string, object>>();

        public GarageConceptContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            try
            {
                Database.SetInitializer(new Initializer());
                this.Configuration.LazyLoadingEnabled = false;
            }
            catch (DataException e)
            {
                if (e.InnerException is DbEntityValidationException)
                {
                    foreach (var eve in (e.InnerException as DbEntityValidationException).EntityValidationErrors)
                    {
                        Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                }
                throw;
            }
        }

        public static GarageConceptContext Create()
        {
            return new GarageConceptContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties<string>().Configure(p => p.HasColumnType("nvarchar"));
            modelBuilder.Properties<string>().Configure(p => p.HasMaxLength(100));

            modelBuilder.Entity<IdentityRole>().Property(r => r.Name).HasMaxLength(50);

            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new OrderConfiguration());
            modelBuilder.Configurations.Add(new VehicleConfiguration());
            modelBuilder.Configurations.Add(new LogConfiguration());
        }

        private async Task<int> GeraLog(Func<Task<int>> saveChangesCallback)
        {
            int result = 0;
            try
            {
                var entries = ChangeTracker.Entries<ILoggable>().Where(e => e.State != EntityState.Unchanged && e.State != EntityState.Detached).ToArray();
                foreach (var entry in entries)
                {
                    // Por padrão, forço esses campos a não estarem modificados...impedindo edição acidental...
                    DbPropertyEntry propRegistro = entry.Entity.GetType().GetProperty("Register") != null ? entry.Property("Register") : null;
                    DbPropertyEntry propCreatorUserId = entry.Entity.GetType().GetProperty("CreatorUserId") != null ? entry.Property("CreatorUserId") : null;

                    switch (entry.State)
                    {
                        case EntityState.Added:
                            // quando é um novo registro, definimos esses campos, se disponíveis...
                            if (propRegistro != null)
                            {
                                propRegistro.CurrentValue = DateTime.Now;
                            }

                            if (propCreatorUserId != null)
                            {
                                propCreatorUserId.CurrentValue = this.CreatorUserId;
                            }

                            break;
                        case EntityState.Deleted:
                            LogDelete(entry);
                            break;
                        case EntityState.Modified:
                            if (propRegistro != null)
                            {
                                propRegistro.IsModified = false;
                            }

                            if (propCreatorUserId != null)
                            {
                                propCreatorUserId.IsModified = false;
                            }

                            if (entry.Entity.GetType().GetProperty("Id", BindingFlags.Public | BindingFlags.NonPublic) != null)
                            {
                                entry.Property("Id").IsModified = false;
                            }

                            LogUpdate(entry);
                            break;
                        default:
                            break;
                    }
                }

                result = await saveChangesCallback().ConfigureAwait(false);
                foreach (var entry in entries)
                {
                    LogChangesAfterSave(entry);
                }

                result += await saveChangesCallback().ConfigureAwait(false);
            }
            catch (DbEntityValidationException ex)
            {
                var erros = new List<string>();
                foreach (var erro in ex.EntityValidationErrors)
                {
                    foreach (var val in erro.ValidationErrors)
                    {
                        erros.Add(string.Format("{0}: {1}", erro.Entry.Entity.GetType().Name, val.ErrorMessage));
                    }
                }
                var mensagemErro = string.Join(" | ", erros);
                throw new InvalidOperationException("Erro ao persistir dados. Detalhes: " + mensagemErro);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            return result;
        }

        public virtual int SaveChanges(string userId, string atividadeId)
        {
            if (userId == null)
            {
                throw new ArgumentException("Não foi possível detectar o userId!!!");
            }

            this.CreatorUserId = userId;
            return this.SaveChanges();
        }

        public virtual async Task<int> SaveChangesAsync(string userId, string atividadeId)
        {
            if (userId == null)
            {
                throw new ArgumentException("Não foi possível detectar o userId!!!");
            }

            this.CreatorUserId = userId;
            return await SaveChangesAsync().ConfigureAwait(false);
        }

        public virtual async Task<int> SaveChangesAsync(string userId, string atividadeId, CancellationToken cancellationToken)
        {
            if (userId == null)
            {
                throw new ArgumentException("Não foi possível detectar o userId!!!");
            }

            this.CreatorUserId = userId;
            return await SaveChangesAsync(cancellationToken).ConfigureAwait(false);
        }

        public async sealed override Task<int> SaveChangesAsync()
        {
            return await base.SaveChangesAsync().ConfigureAwait(false);
        }

        public async sealed override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return await GeraLog(async () => { return await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false); });
        }

        public sealed override int SaveChanges()
        {
            return Task.Run(async () => await GeraLog(() => { return Task.Run(() => base.SaveChanges()); }).ConfigureAwait(false)).GetAwaiter().GetResult();
        }

        private void LogUpdate(DbEntityEntry<ILoggable> entry)
        {
            var props = new Dictionary<string, object>();
            foreach (var property in entry.OriginalValues.PropertyNames)
            {
                var originalValue = entry.OriginalValues[property];
                var currentValue = entry.CurrentValues[property];
                props[property] = currentValue;
                if (!Equals(originalValue, currentValue))
                {
                    AddLog(new Log()
                    {
                        Field = property,
                        EntityName = entry.Entity.GetClassNameForLog(),
                        ObjectId = entry.Entity.Id,
                        UserId = this.CreatorUserId,
                        NewValue = currentValue == null ? null : currentValue.ToString().Limit(0, 4000),
                        OldValue = originalValue == null ? null : originalValue.ToString().Limit(0, 4000)
                    });
                }
            }
            this.ValuesBeforeChange[entry.Entity.GetHashCode()] = props;
        }

        protected virtual void AddLog(Log log)
        {
            Logs.Add(log);
        }

        private void LogChangesAfterSave(DbEntityEntry<ILoggable> entry)
        {
            if (!this.ValuesBeforeChange.ContainsKey(entry.Entity.GetHashCode()))
            {
                return;
            }

            var props = this.ValuesBeforeChange[entry.Entity.GetHashCode()];
            foreach (var property in entry.OriginalValues.PropertyNames)
            {
                var currentValue = entry.CurrentValues[property];
                var originalValue = props[property];
                if (!Equals(originalValue, currentValue))
                {
                    AddLog(new Log()
                    {
                        Field = property,
                        EntityName = entry.Entity.GetClassNameForLog(),
                        ObjectId = entry.Entity.Id,
                        UserId = this.CreatorUserId,
                        NewValue = currentValue == null ? null : currentValue.ToString().Limit(0, 100),
                        OldValue = originalValue == null ? null : originalValue.ToString().Limit(0, 100)
                    });
                }
            }
        }

        public virtual void LogDelete(DbEntityEntry<ILoggable> entry)
        {
            AddLog(new Log()
            {
                Field = "object_deleted",
                EntityName = entry.Entity.GetClassNameForLog(),
                ObjectId = entry.Entity.Id,
                UserId = this.CreatorUserId
            });
        }



    }
}