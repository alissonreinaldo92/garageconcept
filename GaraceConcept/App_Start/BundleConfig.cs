﻿using System.Web;
using System.Web.Optimization;

namespace GarageConcept
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // App Styles
            bundles.Add(new StyleBundle("~/css/app").Include(
                "~/Content/app/css/app.css",
                "~/Content/app/css/garage-concept.css"
            ));


            bundles.Add(new ScriptBundle("~/bundles/GarageConcept").Include(
                // App init
                "~/Scripts/app/app.init.js",
                // Modules
                "~/Scripts/app/modules/bootstrap-start.js",
                "~/Scripts/app/modules/constants.js",
                "~/Scripts/app/modules/clear-storage.js",
                "~/Scripts/app/modules/panel-tools.js",
                "~/Scripts/app/modules/sidebar.js",
                "~/Scripts/app/modules/toggle-state.js",
                "~/Scripts/app/modules/utils.js"
            ));

            RegisterPlugins(bundles);

            // orders
            bundles.Add(new ScriptBundle("~/bundles/pages/orders/index").Include(
                "~/Scripts/app/orders/index.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/pages/orders/details").Include(
                "~/Scripts/app/orders/details.js"
            ));

            // vehicles
            bundles.Add(new ScriptBundle("~/bundles/pages/vehicles/index").Include(
                "~/Scripts/app/vehicles/index.js"
            ));

            // images
            bundles.Add(new ScriptBundle("~/bundles/pages/images/add").Include(
                "~/Scripts/app/images/add.js"
            ));

        }


        public static void RegisterPlugins(BundleCollection bundles)
        {

            // Animate
            bundles.Add(new StyleBundle("~/css/animate").Include(
                "~/Content/plugins/animate/animate.min.css"
            ));

            // Bootgrid
            bundles.Add(new ScriptBundle("~/bundles/bootgrid").Include(
                "~/Scripts/plugins/bootgrid/jquery.bootgrid.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootgrid-fa").Include(
                "~/Scripts/plugins/bootgrid/jquery.bootgrid.fa.js"));

            bundles.Add(new StyleBundle("~/css/bootgrid").Include(
                "~/Content/plugins/bootgrid/*.css"
            ));

            // Bootstrap
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/plugins/bootstrap/*.js"));

            bundles.Add(new StyleBundle("~/css/bootstrap").Include(
                "~/Content/plugins/bootstrap/bootstrap.css", new CssRewriteUrlTransform()
            ));

            // Chosen
            bundles.Add(new ScriptBundle("~/bundles/chosen").Include(
                "~/Scripts/plugins/chosen/*.js"));

            bundles.Add(new StyleBundle("~/css/chosen").Include(
                "~/Content/plugins/chosen/*.css"
            ));

            // File Upload
            bundles.Add(new StyleBundle("~/css/fileupload").Include(
                "~/Content/plugins/blueimp-file-upload/css/jquery.fileupload.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/fileupload").Include(
                "~/Scripts/plugins/jquery-ui/jquery-ui.js",
                "~/Scripts/plugins/jquery-ui/ui/widget.js",
                "~/Scripts/plugins/blueimp-tmpl/tmpl.js",
                "~/Scripts/plugins/blueimp-load-image/load-image.all.min.js",
                "~/Scripts/plugins/blueimp-canvas-to-blob/canvas-to-blob.js",
                "~/Scripts/plugins/blueimp-file-upload/jquery.iframe-transport.js",
                "~/Scripts/plugins/blueimp-file-upload/jquery.fileupload.js",
                "~/Scripts/plugins/blueimp-file-upload/jquery.fileupload-process.js",
                "~/Scripts/plugins/blueimp-file-upload/jquery.fileupload-image.js",
                "~/Scripts/plugins/blueimp-file-upload/jquery.fileupload-audio.js",
                "~/Scripts/plugins/blueimp-file-upload/jquery.fileupload-video.js",
                "~/Scripts/plugins/blueimp-file-upload/jquery.fileupload-validate.js",
                "~/Scripts/plugins/blueimp-file-upload/jquery.fileupload-ui.js"
            ));

            // font awesome
            bundles.Add(new StyleBundle("~/css/fontawesome").Include(
              "~/Content/plugins/fontawesome/css/font-awesome.min.css", new CssRewriteUrlTransform()
            ));

            // Input Mask
            bundles.Add(new ScriptBundle("~/bundles/inputmask").Include(
                "~/Scripts/plugins/jquery.inputmask/jquery.inputmask.bundle.js"
            ));

            // jQuery
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/plugins/jquery/*.js",
                "~/Scripts/plugins/jquery-storage/*.js"
            ));

            // Modernizr
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/plugins/modernizr/modernizr-*"));

            // Simple Line Icons
            bundles.Add(new StyleBundle("~/css/simpleLineIcons").Include(
                "~/Content/plugins/simple-line-icons/css/simple-line-icons.css", new CssRewriteUrlTransform()
            ));

            // Sweet Alert
            bundles.Add(new ScriptBundle("~/bundles/sweet-alert").Include(
                "~/Scripts/plugins/sweetalert/sweetalert.min.js"));

            bundles.Add(new StyleBundle("~/css/sweet-alert").Include(
                "~/Content/plugins/sweetalert/*.css"
            ));

            // Toastr
            bundles.Add(new ScriptBundle("~/bundles/toastr").Include(
                "~/Scripts/plugins/toastr/toastr.min.js"));

            bundles.Add(new StyleBundle("~/css/toastr").Include(
                "~/Content/plugins/toastr/toastr.min.css"
            ));

            // Whirl
            bundles.Add(new StyleBundle("~/css/whirl").Include(
                "~/Content/plugins/whirl/whirl.css"
            ));

        }

    }
}
