﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GarageConcept.ViewModels
{
    public class BootgridRequestViewModel
    {
        public int current { get; set; }
        public int rowCount { get; set; }
        public string searchPhrase { get; set; }
        public IEnumerable<SortData> sortItems { get; set; }
    }
}