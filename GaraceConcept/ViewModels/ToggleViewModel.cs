﻿namespace GarageConcept.ViewModels
{
    public class ToggleViewModel
    {

        public int ObjectId { get; set; }

        public bool Value { get; set; }

    }

    public class StringToggleViewModel
    {

        public string ObjectId { get; set; }

        public bool Value { get; set; }

    }

}