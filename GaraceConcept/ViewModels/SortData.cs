﻿namespace GarageConcept.ViewModels
{
    public class SortData
    {
        public string Field { get; set; } // FIeld Name
        public string Type { get; set; } // ASC or DESC
    }
}