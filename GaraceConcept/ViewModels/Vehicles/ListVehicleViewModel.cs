﻿using GarageConcept.Models;
using System;

namespace GarageConcept.ViewModels.Vehicles
{
    public class ListVehicleViewModel
    {
        public int Id { get; internal set; }
        public DateTime Register { get; internal set; }
        public string RegisterDisplay { get; internal set; }
        public string PlateNumber { get; internal set; }
        public string ChassisNumber { get; internal set; }
        public string Model { get; internal set; }
        public string OwnerName { get; internal set; }
        public string OwnerCpf { get; internal set; }
        public string OwnerEmail { get; internal set; }
        public int OSCount { get; internal set; }

        public void Format()
        {
            this.RegisterDisplay = this.Register.ToString("dd/MM/yyyy HH:mm");
        }

    }
}