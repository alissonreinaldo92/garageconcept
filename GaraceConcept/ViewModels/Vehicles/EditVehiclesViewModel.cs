﻿using GarageConcept.Helpers.Attributes.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GarageConcept.ViewModels.Vehicles
{
    public class EditVehiclesViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Por favor, preencha a placa!")]
        [DisplayName("Placa")]
        [RegexReplace(ReplaceThis = "-", WithThis = "")]
        public string PlateNumber { get; set; }

        [Required(ErrorMessage = "Por favor, preencha o chassi!")]
        [DisplayName("Chassi")]
        [MaxLength(17, ErrorMessage = "Um chassi deve ter no máximo {1} caracteres.")]
        public string Chassis { get; set; }

        [Required(ErrorMessage = "Por favor, preencha o model!")]
        [DisplayName("Modelo")]
        public string VehicleModel { get; set; }

        [Required(ErrorMessage = "Por favor, digite o nome do proprietário!")]
        [DisplayName("Proprietário")]
        public string OwnerName { get; set; }

        [Required(ErrorMessage = "Por favor, digite o CPF do proprietário!")]
        [DisplayName("CPF Proprietário")]
        [RegexReplace(ReplaceThis = ".-", WithThis = "")]
        public string OwnerCpf { get; set; }
        
    }
}