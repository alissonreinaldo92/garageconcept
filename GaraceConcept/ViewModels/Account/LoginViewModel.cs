﻿using GarageConcept.Helpers.Attributes.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GarageConcept.ViewModels.Account
{

    public class LoginViewModel
    {

        [Required]
        [DisplayName("LoginViewModel_Login")]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Common_Senha")]
        public string Password { get; set; }

        [DisplayName("LoginViewModel_RememberMe")]
        public bool RememberMe { get; set; }
    }

}