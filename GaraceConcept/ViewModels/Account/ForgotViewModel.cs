﻿using GarageConcept.Helpers.Attributes.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GarageConcept.ViewModels.Account
{

    public class ForgotViewModel
    {
        [Required]
        [DisplayName("Common_Email")]
        [EmailAddress]
        public string Email { get; set; }
    }

    public class ResetPasswordViewModel
    {

        [Required]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "Digite uma senha de 6 a 100 caracteres.")]
        [DataType(DataType.Password)]
        [DisplayName("Common_Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("Common_ConfirmPass")]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        [DisplayName("Common_Code")]
        public string Code { get; set; }

        public string UserId { get; set; }
    }

    public class EmailViewModel
    {
        [Required]
        [EmailAddress]
        [DisplayName("Common_Email")]
        public string Email { get; set; }
    }

}