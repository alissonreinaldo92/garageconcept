﻿using GarageConcept.Helpers.Attributes.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GarageConcept.ViewModels.Account
{

    public class RegisterViewModel
    {

        [Required]
        [DisplayName("Common_Login")]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        [DisplayName("Common_Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "")]
        [DataType(DataType.Password)]
        [DisplayName("Common_Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("Common_ConfirmPass")]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        public bool UserAccepts { get; set; }

    }

}