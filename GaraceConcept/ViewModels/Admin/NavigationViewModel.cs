﻿using System.Collections.Generic;

namespace GarageConcept.ViewModels.Admin
{
    public class NavigationViewModel
    {

        public string UserName { get; set; }
        public bool IsAdmin { get; set; }

    }
}
