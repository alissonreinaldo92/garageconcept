﻿using System.Collections.Generic;

namespace GarageConcept.ViewModels.Orders
{
    public class OrderDetailsViewModel
    {
        public int Id { get; set; }

        public string PlateNumber { get; set; }

        public string Chassis { get; set; }

        public string VehicleModel { get; set; }

        public string OwnerName { get; set; }

        public string OwnerCpf { get; set; }

        public string OwnerPhone { get; set; }

        public string OwnerEmail { get; set; }

        public string Description { get; set; }

        public string OwnerId { get; set; }

        public bool IsAdmin { get; set; }

        public IEnumerable<int> Images { get; set; }
    }
}