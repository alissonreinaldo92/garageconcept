﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GarageConcept.ViewModels.Orders
{
    public class GeneratePasswordViewModel
    {

        public string PlateNumber { get; set; }

        [Required(ErrorMessage = "Por favor, preencha uma senha.")]
        [MinLength(6, ErrorMessage = "A senha deve conter no mínimo 6 caracteres, ao menos uma letra minúscula e um número.")]
        [DisplayName("Senha")]
        public string Password { get; set; }

        public int OrderId { get; set; }

    }
}