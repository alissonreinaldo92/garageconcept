﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GarageConcept.ViewModels.Images
{
    public class ImageViewModel
    {
        public string DeleteType { get; set; }
        public string Name { get; set; }
        public int Size { get; set; }
        public string Url { get; set; }
        public string ThumbnailUrl { get; set; }
        public string DeleteUrl { get; set; }
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}