﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GarageConcept.Models.Interfaces
{
    public interface ILoggable
    {

        string Id { get; }

        string GetClassNameForLog();
    }
}