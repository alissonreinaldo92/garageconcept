﻿using GarageConcept.Helpers.Attributes.DataAnnotations;

namespace GarageConcept.Models
{
    public enum Roles
    {
        [StringValue("ADMIN")]
        SiteAdmin
    }

    public enum OrderStatus
    {
        [StringValue("Nova")]
        New,
        [StringValue("Excluída")]
        Deleted
    }

    public enum VehicleStatus
    {
        [StringValue("Novo")]
        New,
        [StringValue("Excluída")]
        Deleted
    }

}