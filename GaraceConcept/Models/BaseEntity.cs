﻿using GarageConcept.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GarageConcept.Models
{
    public abstract class BaseEntity: ILoggable
    {

        public int Id { get; protected set; }

        public DateTime Register { get; protected set; }

        string ILoggable.Id => this.Id.ToString();

        public BaseEntity()
        {
            this.Register = DateTime.Now;
        }

        protected Type GetUnderlyingType()
        {
            var thisType = this.GetType();

            if (thisType.Namespace == "System.Data.Entity.DynamicProxies")
                return thisType.BaseType;

            return thisType;
        }

        public string GetClassNameForLog()
        {
            var type = this.GetUnderlyingType();
            return type.Name;
        }

    }
}