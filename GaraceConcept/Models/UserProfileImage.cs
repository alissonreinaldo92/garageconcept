﻿namespace GarageConcept.Models
{
    public class UserProfileImage
    {

        public int ID { get; private set; }
        public virtual User Owner { get; protected set; }
        public byte[] Data { get; set; }

        public UserProfileImage()
        {

        }

        public UserProfileImage(byte[] imageData)
        {
            this.Data = imageData;
        }

    }
}
