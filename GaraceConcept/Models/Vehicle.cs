﻿using GarageConcept.Models;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GarageConcept.Models
{
    public class Vehicle: BaseEntity
    {

        public string PlateNumber { get; set; }

        public string ChassisNumber { get; set; }

        public string Model { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public string OwnerId { get; set; }

        public virtual User Owner { get; set; }

        public Vehicle()
        {
            this.Orders = new HashSet<Order>();
        }

        public Vehicle(string plate): this()
        {
            this.PlateNumber = plate;
        }

    }
}