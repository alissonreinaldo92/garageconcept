﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GarageConcept.Models.Interfaces;

namespace GarageConcept.Models
{

    public class User : IdentityUser, ILoggable
    {
        
        public string Name { get; set; }
        public string Cpf { get; set; }
        public string PictureUrl { get; set; }
        public DateTime? Birthday { get; set; }
        public string Skype { get; set; }
        public string TwitterUrl { get; set; }
        public string FacebookUrl { get; set; }
        public string GooglePlusUrl { get; set; }
        public string Whatsapp { get; set; }
        public string LinkedinUrl { get; set; }
        public DateTime RegisterDate { get; protected set; }
        public DateTime UpdateDate { get; protected set; }

        public virtual ICollection<Vehicle> Vechiles { get; set; }

        public virtual UserProfileImage ProfileImage { get; set; }

        protected User()
        {
            this.RegisterDate = DateTime.Now;
            this.UpdateDate = DateTime.Now;
            this.Vechiles = new HashSet<Vehicle>();
        }

        public User(string cpf): this()
        {
            this.Cpf = cpf;
            this.Email = $"{cpf}@example.com";
            this.UserName = cpf;
        }

        private void SetProfileImage(byte[] imageData)
        {
            if (imageData == null)
                return;
            this.PictureUrl = null;
            if (this.ProfileImage == null)
                this.ProfileImage = new UserProfileImage(imageData);
            else
                this.ProfileImage.Data = imageData;

        }

        public void SetThumbnail(string base64)
        {
            string normalizedBase64 = base64.NormalizeBase64String();
            if (string.IsNullOrEmpty(normalizedBase64))
                return;
            byte[] data = Convert.FromBase64String(base64.NormalizeBase64String());
            if (data == null || data.Length == 0)
                return;
            this.SetProfileImage(data);
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
    {
        var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
        return userIdentity;
    }

        public byte[] GetThumbnailData()
        {
            return this.ProfileImage == null ? null : this.ProfileImage.Data;
        }

        public string GetClassNameForLog()
        {
            return "User";
        }
    }

}