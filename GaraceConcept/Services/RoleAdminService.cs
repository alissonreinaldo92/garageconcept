﻿using GarageConcept.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace GarageConcept.Services
{
    public static class RoleAdminService
    {

        private static readonly object lockObj = new object();

        public static bool HasRole(string userId, string role)
        {
            lock (lockObj)
            {
                if (HttpContext.Current.Cache[role] == null)
                    HttpContext.Current.Cache[role] = new Dictionary<string, bool>();
                if (!(HttpContext.Current.Cache[role] as Dictionary<string, bool>).ContainsKey(userId))
                {
                    bool isInRole = AuthHelper.GetUserManager().IsInRole(userId, role);
                    (HttpContext.Current.Cache[role] as Dictionary<string, bool>).Add(userId, isInRole);
                }
                return (HttpContext.Current.Cache[role] as Dictionary<string, bool>)[userId];
            }
        }

        public static void UpdateRoleForUser(string userId, string role, bool value)
        {
            lock (lockObj)
            {
                (HttpContext.Current.Cache[role] as Dictionary<string, bool>)[userId] = value;
            }
        }

    }
}