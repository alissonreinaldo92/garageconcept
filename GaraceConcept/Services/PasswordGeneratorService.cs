﻿using System;
using System.Text;

namespace GarageConcept.Services
{
    public class PasswordGeneratorService
    {

        public static string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyz1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

    }
}