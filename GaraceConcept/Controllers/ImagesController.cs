﻿using GarageConcept.Data.Context;
using GarageConcept.ViewModels.Images;
using GarageConcept.ViewModels.Orders;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace GarageConcept.Controllers
{

    public class ImagesController : Controller
    {
        
        private const int ThumbSize = 160;

        [Authorize(Roles = "ADMIN")]
        public ActionResult Add(int id)
        {
            using (var db = new GarageConceptContext())
            {
                var model = db.Orders.Where(o => o.Id == id)
                    .Select(o => new AddImagesViewModel
                    {
                        Id = o.Id,
                    })
                    .FirstOrDefault();
                return View(model);

            }
        }

        public async Task<ActionResult> GetFile(int id, bool thumbnail = false)
        {
            using (var db = new GarageConceptContext())
            {
                var file = await db.OrderImages.FindAsync(id);
                return thumbnail ? Thumb(file.Data, "image/png") : File(file.Data, "image/png");
            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public async Task<ActionResult> DeleteFile(int id)
        {
            using (var db = new GarageConceptContext())
            {
                var file = await db.OrderImages.FindAsync(id);
                db.OrderImages.Remove(file);
                await db.SaveChangesAsync();
                return Json($"Imagem [{file.FileName}] excluída");
            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet]
        public async Task<ActionResult> Upload(string[] names)
        {
            using (var db = new GarageConceptContext())
            {
                var files = await db.OrderImages
                    .Where(i => names.Contains(i.FileName))
                    .Select(file => new ImageViewModel
                    {
                        Id = file.Id,
                        DeleteType = "POST",
                        Name = file.FileName,
                        Size = file.DataLength
                    }).ToListAsync();

                foreach (var file in files)
                {
                    file.Url = Url.Action("GetFile", "Images", new { file.Id });
                    file.ThumbnailUrl = Url.Action("GetFile", "Images", new { file.Id, thumbnail = true });
                    file.DeleteUrl = Url.Action("DeleteFile", "Images", new { file.Id });
                }

                return Json(new
                {
                    files
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public async Task<ActionResult> Upload(int id)
        {
            var files = Request.Files
                .Cast<string>()
                .Select(k => Request.Files[k])
                .ToArray();

            using (var db = new GarageConceptContext())
            {
                var order = await db.Orders.FindAsync(id);
                foreach (var file in files)
                    order.AddImage(file.FileName, file.ToByteArray());
                await db.SaveChangesAsync();
            }
            
            var names = files.Select(f => f.FileName);
            return await Upload(names.ToArray());
        }

        private ActionResult Thumb(byte[] data, string contentType)
        {
            if (contentType.StartsWith("image"))
            {
                using (var ms = new MemoryStream(data))
                using (var img = Image.FromStream(ms))
                {
                    var thumbHeight = (int)(img.Height * (ThumbSize / (double)img.Width));
                    var thumb = data.FixImageSize(ThumbSize, thumbHeight);
                    return File(thumb, contentType);
                }
            }

            return Redirect($"https://placehold.it/{ThumbSize}?text={Url.Encode("png")}");
        }

    }
}