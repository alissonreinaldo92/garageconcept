﻿using GarageConcept.Helpers;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;

namespace GarageConcept.Controllers
{
    public class BaseController : Controller
    {

        protected override ViewResult View(IView view, object model)
        {
            if (TempData["ToastrMessage"] != null)
                ViewBag.ToastrMessage = TempData["ToastrMessage"];
            ViewBag.IsAdmin = AuthHelper.IsAdmin();
            return base.View(view, model);
        }

        protected override ViewResult View(string viewName, string masterName, object model)
        {
            if (TempData["ToastrMessage"] != null)
                ViewBag.ToastrMessage = TempData["ToastrMessage"];
            ViewBag.IsAdmin = AuthHelper.IsAdmin();
            return base.View(viewName, masterName, model);
        }

        protected ActionResult RedirectToLocal(string returnUrl = null)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return Redirect(Url.Content("~/"));
        }

        protected void AddToastrMessage(ToastrStatus status, string message)
        {
            TempData["ToastrMessage"] = new ToastrMessage(status, message);
        }

        protected void AddError(string error)
        {
            ModelState.AddModelError("", error);
        }

        protected void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
                AddError(error);
        }

    }
}