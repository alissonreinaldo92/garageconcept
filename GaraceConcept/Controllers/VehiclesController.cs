﻿using GarageConcept.Data.Context;
using GarageConcept.Models;
using GarageConcept.ViewModels.Vehicles;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GarageConcept.Controllers
{
    [Authorize(Roles = "ADMIN")]
    public class VehiclesController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            using (var db = new GarageConceptContext())
            {
                var model = await db.Vehicles.Where(o => o.Id == id)
                    .Select(o => new EditVehiclesViewModel
                    {
                        Id = o.Id,
                        PlateNumber = o.PlateNumber,
                        Chassis = o.ChassisNumber,
                        OwnerCpf = o.Owner.Cpf,
                        OwnerName = o.Owner.Name,
                        VehicleModel = o.Model
                    })
                    .FirstOrDefaultAsync();
                return View(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditVehiclesViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (var db = new GarageConceptContext())
                {
                    bool exists = await db.Vehicles.Where(v => v.PlateNumber == model.PlateNumber && v.Id != model.Id).AnyAsync();
                    if (exists)
                    {
                        ModelState.AddModelError(string.Empty, "Já existe um veículo cadastrado com essa placa.");
                        return View(model);
                    }
                    exists = await db.Vehicles.Where(v => v.ChassisNumber == model.Chassis && v.Id != model.Id).AnyAsync();
                    if (exists)
                    {
                        ModelState.AddModelError(string.Empty, "Já existe um veículo cadastrado com esse chassi.");
                        return View(model);
                    }
                    var vehicle = await db.Vehicles.Include(o => o.Owner).FirstOrDefaultAsync(o => o.Id == model.Id);
                    vehicle.PlateNumber = model.PlateNumber;
                    vehicle.ChassisNumber = model.Chassis;
                    vehicle.Model = model.VehicleModel;
                    if (vehicle.Owner.Cpf != model.OwnerCpf)
                    {
                        var owner = await db.Users.FirstOrDefaultAsync(u => u.Cpf == model.OwnerCpf) ?? new User(model.OwnerCpf);
                        owner.Name = model.OwnerName;
                        vehicle.Owner = owner;
                    }
                    await db.SaveChangesAsync();
                }
                return RedirectToAction("Index");
            }
            return View(model);
        }

    }
}