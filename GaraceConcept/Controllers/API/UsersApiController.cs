﻿using GarageConcept.Data.Context;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GarageConcept.Controllers.API
{
    [Authorize(Roles = "ADMIN")]
    [RoutePrefix("api/users")]
    public class UsersApiController : ApiController
    {


        [HttpGet]
        [Route("{cpf}/details", Name = "Users.GetDetails")]
        public async Task<IHttpActionResult> GetDetails(string cpf)
        {
            using (var db = new GarageConceptContext())
            {
                var user = await db.Users.Where(v => v.Cpf == cpf).Select(v => new
                {
                    v.Name
                }).FirstOrDefaultAsync();

                return Ok(user);
            }
        }
    }
}