﻿$(function () {

    var getUserDetailsUrl = $("#urls").data("user-details-url");

    $(".cpf").keyup(function () {
        var cpf = $(this).inputmask('unmaskedvalue');
        if (cpf.length === 11) {
            $.ajax({
                url: getUserDetailsUrl.replace("99999999999", cpf),
                method: "GET"
            })
                .done(function (data) {
                    $("#OwnerName").val(data.Name);
                });
        }
    });

});