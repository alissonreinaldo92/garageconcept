﻿$(function () {

    var getUrl = $("#grid-vehicles").data("get-url");
    var deleteUrl = $("#grid-vehicles").data("delete-url");
    var editUrl = $("#grid-vehicles").data("edit-url");

    var grid = $("#grid-vehicles").bootgrid({
        ajax: true,
        rowCount: [10, 25, 50, 100],
        labels: {
            all: "Todos",
            infos: "Mostrando de {{ctx.start}} a {{ctx.end}} para um total de {{ctx.total}} Veículos",
            loading: "Carregando...",
            refresh: "Atualizar",
            search: "Pesquisar",
            noResults: "Nenhum veículo encontrado..."
        },
        requestHandler: function (request) {
            request.sortItems = [];
            if (request.sort === null)
                return request;
            for (var property in request.sort) {
                if (request.sort.hasOwnProperty(property)) {
                    request.sortItems.push({ Field: property, Type: request.sort[property] });
                }
            }
            return request;
        },
        ajaxSettings: {
            method: "POST",
            cache: false
        },
        searchSettings: {
            delay: 250,
            characters: 3
        },
        url: getUrl,
        formatters: {
            "vehicle": function (column, row) {
                return `
                    <div class="row">
                        <div class="col-xs-12">
                            <span data-masked="" data-inputmask="'mask': 'aaa-9999'">${row.PlateNumber}</span> | ${row.Model}
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="row">
                            Chassi: ${row.ChassisNumber}
                        </div>
                    </div>
                `;
            },
            "owner": function (column, row) {
                console.log(row);
                return `
                    <div class="row">
                        <div class="col-xs-12">
                            ${row.OwnerName} - CPF: <span data-masked="" data-inputmask="'mask': '999.999.999-99'"> ${row.OwnerCpf}</span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="row">
                            ${row.OwnerEmail}
                        </div>
                    </div>
                `;
            },
            "commands": function (column, row) {
                var str = `
                <button type="button" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-xs btn-info command-edit" data-row-id="${row.Id}"><span class="fa fa-edit"></span></button>
                <button type="button" data-toggle="tooltip" data-placement="top" title="Excluir" class="btn btn-xs btn-danger command-delete" data-row-id="${row.Id}"><span class="fa fa-trash"></span></button>
                `;
                return str;
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {

        $('[data-toggle="tooltip"]').tooltip();

        if ($.fn.inputmask)
            $('[data-masked]').inputmask();

        grid.find(".command-edit").mouseup(function (e) {
            var url = editUrl;
            result = url.substring(0, url.lastIndexOf("/"));
            url = result + '/' + $(this).data("row-id");
            if (e.which === 1)
                document.location = url;
            else if (e.which === 2)
                window.open(url, "_blank");
        });

        grid.find(".command-delete").mouseup(function (e) {
            var url = deleteUrl;
            url = url.substring(0, url.lastIndexOf("/"));
            url = url + '/' + $(this).closest('tr').data("row-id");
            swal({
                title: "Deseja excluir esse Veículo?",
                text: "",
                type: "warning",
                showCancelButton: true,
                //confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, desejo excluir!",
                cancelButtonText: "Não!",
                closeOnConfirm: true,
                closeOnCancel: true
            },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            method: "DELETE"
                        })
                            .done(function () {
                                grid.bootgrid('reload');
                                toastr.success("Veículo excluído com sucesso!");
                            })
                            .fail(function (xhr) {
                                var response = JSON.parse(xhr.responseText);
                                toastr.error(response.ExceptionMessage);
                            });
                    }
                });
        });

    });


});