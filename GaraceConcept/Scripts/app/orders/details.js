﻿$(function () {

    var getImagesUrl = $("#urls").data("url-get-images");
    var getHistoryUrl = $("#grid-history").data("get-url");

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        if ($('.order-images').children().length > 0)
            return;
        var target = $(e.target).attr("href") // activated tab
        if (target === '#history')
            loadHistoric();
    });

    function loadImages() {
        $.ajax({
            url: getImagesUrl,
            success: function (data) {
                $('.order-images').empty();
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    console.log(item);
                    $('.order-images').append(
                        ` 
                            <div class="col-masonry text-center">
                                <div data-image-delete-url="${item.DeleteUrl}" class="img-wrap">
                                    <span class="close delete-image">&times;</span>
                                    <img src="${item.ThumbnailUrl}" alt="${item.Name}" class="img-responsive" />
                                </div>
                                <p class="label label-primary"><i class="fa fa-calendar"></i> ${moment(item.CreatedAt).format('DD/MM/YYYY HH:mm:ss')}</p>
                            </div>
                        `
                    )
                }
            }
        });
    }

    function loadHistoric() {
        //grid.bootgrid("reload");
    }

    var grid = $("#grid-history").bootgrid({
        ajax: true,
        rowCount: [10, 25, 50, 100],
        labels: {
            all: "Todos",
            infos: "Mostrando de {{ctx.start}} a {{ctx.end}} para um total de {{ctx.total}} atualizações no histórico.",
            loading: "Carregando...",
            refresh: "Atualizar",
            search: "Pesquisar",
            noResults: "Nada encontrado..."
        },
        requestHandler: function (request) {
            request.sortItems = [];
            if (request.sort === null)
                return request;
            for (var property in request.sort) {
                if (request.sort.hasOwnProperty(property)) {
                    request.sortItems.push({ Field: property, Type: request.sort[property] });
                }
            }
            return request;
        },
        ajaxSettings: {
            method: "POST",
            cache: true
        },
        searchSettings: {
            delay: 250,
            characters: 3
        },
        url: getHistoryUrl,
        formatters: {
            "date": function (column, row) {
                return moment(row.CreatedAt).format('DD/MM/YYYY HH:mm:ss');
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {

    });

    $(document).on('click', '.delete-image', function () {
        var deleteUrl = $(this).parent().data('image-delete-url');
        swal({
            title: "Deseja excluir essa imagem?",
            text: "",
            type: "warning",
            showCancelButton: true,
            //confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, desejo excluir!",
            cancelButtonText: "Não!",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: deleteUrl,
                        method: "POST"
                    })
                        .done(function () {
                            loadImages();
                            toastr.success("Imagem excluída com sucesso!");
                        })
                        .fail(function () {
                            toastr.error("Não foi possível excluir essa imagem!");
                        });
                }
            });
    });

    loadImages();

});