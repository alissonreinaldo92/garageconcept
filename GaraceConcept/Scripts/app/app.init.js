/*!
 *
 * Angle - Bootstrap Admin App + MVC
 *
 * Version: 3.8.7
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: https://wrapbootstrap.com/help/licenses
 *
 */


(function (window, document, $, undefined) {

    if (typeof $ === 'undefined') {
        throw new Error('This application\'s JavaScript requires jQuery');
    }

    $(function () {

        // Restore body classes
        // -----------------------------------
        var $body = $('body');
        new StateToggler().restoreState($body);

        // enable settings toggle after restore
        $('#chk-fixed').prop('checked', $body.hasClass('layout-fixed'));
        $('#chk-collapsed').prop('checked', $body.hasClass('aside-collapsed'));
        $('#chk-collapsed-text').prop('checked', $body.hasClass('aside-collapsed-text'));
        $('#chk-boxed').prop('checked', $body.hasClass('layout-boxed'));
        $('#chk-float').prop('checked', $body.hasClass('aside-float'));
        $('#chk-hover').prop('checked', $body.hasClass('aside-hover'));

        // When ready display the offsidebar
        $('.offsidebar.hide').removeClass('hide');


        if ($(".chosen-select").length) {
            $(".chosen-select").chosen({ allow_single_deselect: true });
        }

        $('[data-toggle="tooltip"]').tooltip();

        toastr.options.progressBar = true;

        if ($("#toastr-message").length) {
            var status = $("#toastr-message").data("status");
            var message = $("#toastr-message").data("message");
            toastr[status](message);
        }

        // CHOSEN
        // ----------------------------------- 
        if ($.fn.chosen)
            $('.chosen-select').chosen();

        // MASKED
        // ----------------------------------- 
        if ($.fn.inputmask)
            $('[data-masked]').inputmask();

        // Disable warning "Synchronous XMLHttpRequest on the main thread is deprecated.."
        $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
            options.async = true;
        });

    }); // doc ready

})(window, document, window.jQuery);
