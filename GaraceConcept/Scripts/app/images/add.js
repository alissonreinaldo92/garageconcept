﻿
(function (window, document, $, undefined) {

    $(function () {
        'use strict';

        // Initialize the jQuery File Upload widget:
        $('#fileupload').fileupload({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            // url: 'server/upload'
        });
        
    });

})(window, document, window.jQuery);