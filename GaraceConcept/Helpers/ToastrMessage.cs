﻿using System;

namespace GarageConcept.Helpers
{

    public enum ToastrStatus
    {
        Success,
        Info,
        Warning,
        Error
    }

    public class ToastrMessage
    {
        public string Status { get; set; }
        public string Message { get; set; }

        public ToastrMessage(ToastrStatus status, string message)
        {
            switch (status)
            {
                case ToastrStatus.Success:
                    this.Status = "success";
                    break;
                case ToastrStatus.Info:
                    this.Status = "info";
                    break;
                case ToastrStatus.Warning:
                    this.Status = "warning";
                    break;
                case ToastrStatus.Error:
                    this.Status = "error";
                    break;
                default:
                    throw new ArgumentOutOfRangeException("ToastrStatus");
            }
            this.Message = message;
        }

    }
}