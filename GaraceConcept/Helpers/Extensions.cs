﻿using GarageConcept.Helpers.Attributes.DataAnnotations;
using Microsoft.Owin.Security;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Threading.Tasks;
using GarageConcept.ViewModels;
using System.Linq.Dynamic;
using PagedList;
using System.Web.Mvc;
using System.Web.Routing;
using PagedList.EntityFramework;

namespace System
{
    public static class Extensions
    {

        public static byte[] FixImageSize(this byte[] imageData, int width, int height)
        {
            if (imageData == null)
                return null;
            if (imageData.Length == 0)
                return imageData;
            using (MemoryStream myMemStream = new MemoryStream(imageData))
            {
                Drawing.Image fullsizeImage = Drawing.Image.FromStream(myMemStream);
                if (width == 0 || width > fullsizeImage.Width)
                    width = fullsizeImage.Width;
                if (height == 0 || height > fullsizeImage.Height)
                    height = fullsizeImage.Height;
                Drawing.Image newImage = fullsizeImage.GetThumbnailImage(width, height, null, IntPtr.Zero);
                using (MemoryStream myResult = new MemoryStream())
                {
                    newImage.Save(myResult, Drawing.Imaging.ImageFormat.Png);
                    return myResult.ToArray();
                }
            }
        }

        private static Random rnd = new Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static void ForEach<T>(this IEnumerable<T> array, Action<T> func) where T : IEnumerable
        {
            foreach (var item in array)
                func(item);
        }

        public static T GetRandomItem<T>(this IList<T> list)
        {
            int index = rnd.Next(1, list.Count);
            return list[index];
        }

        public static byte[] ToByteArray(this HttpPostedFileBase file)
        {
            if (file == null)
                return null;
            using (Stream inputStream = file.InputStream)
            {
                MemoryStream memoryStream = inputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    inputStream.CopyTo(memoryStream);
                }
                return memoryStream.ToArray();
            }
        }

        public static async Task<BootgridResponseData<T>> ApplyFilter<T>(this IOrderedQueryable<T> query, BootgridRequestViewModel filters) where T : class
        {
            IQueryable<T> q = query;
            if (filters.sortItems != null && filters.sortItems.Count() > 0)
            {
                string order = filters.sortItems.GetMergedOrderingClause();
                q = q.OrderBy(order);
            }

            var paged = await q.ToPagedListAsync(filters.current, filters.rowCount).ConfigureAwait(false);

            return new BootgridResponseData<T>()
            {
                current = filters.current,
                rowCount = filters.rowCount,
                rows = paged,
                total = paged.TotalItemCount
            };
        }

        public static string GetMergedOrderingClause(this IEnumerable<SortData> sort)
        {
            return string.Join(",", sort.Select(item => String.Format("{0} {1}", item.Field, item.Type)));
        }

        public static string GetMergedOrderingClause<TKey, TValue>(this IDictionary<TKey, TValue> sort)
        {
            return string.Join(",", sort.Select(item => String.Format("{0} {1}", item.Key, item.Value)));
        }

        public static string ToBase64String(this byte[] bytes)
        {
            var base64 = bytes == null ? "" : Convert.ToBase64String(bytes);
            return base64.IsNullOrEmpty() ? "" : String.Format("data:image/gif;base64,{0}", base64);
        }

        public static string GetSocialButtonClass(this AuthenticationDescription auth)
        {
            if (auth.AuthenticationType.Equals("Facebook"))
                return "btn btn-social btn-facebook btn-block";
            if (auth.AuthenticationType.Equals("Google"))
                return "btn btn-social btn-google btn-block";
            return "btn btn-default btn-block";
        }

        public static string GetSocialFontAwesome(this AuthenticationDescription auth)
        {
            if (auth.AuthenticationType.Equals("Facebook"))
                return "fa fa-facebook";
            if (auth.AuthenticationType.Equals("Google"))
                return "fa fa-google";
            return "fa";
        }

        /// <summary>
        /// Will get the string value for a given enums value, this will
        /// only work if you assign the StringValue attribute to
        /// the items in your enum.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetStringValue(this Enum value)
        {
            // Get the type
            Type type = value.GetType();

            // Get fieldinfo for this type
            FieldInfo fieldInfo = type.GetField(value.ToString());

            // Get the stringvalue attributes
            StringValueAttribute[] attribs = fieldInfo.GetCustomAttributes(
                typeof(StringValueAttribute), false) as StringValueAttribute[];

            // Return the first if there was a match.
            return attribs.Length > 0 ? attribs[0].StringValue : null;
        }

        public static Task<TResult> AsTaskResult<TResult>(this TResult result)
        {
            return Task.FromResult(result);
        }

        public static IEnumerable<T> GetValues<T>() where T : struct, IConvertible
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static List<SelectListItem> GetSelectListItem<T>(Func<T,string> getText) where T : struct, IConvertible
        {
            return Enum.GetValues(typeof(T)).Cast<T>().Select(e => new SelectListItem { Value = Convert.ToInt32(e).ToString(), Text = getText(e) }).ToList();
        }


        /// <summary>
        /// Will get the string value for a given enums value, this will
        /// only work if you assign the StringValue attribute to
        /// the items in your enum.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Dictionary<string, int> GetStringValues<T>() where T : struct, IConvertible
        {
            // Get the type
            Type type = typeof(T);

            if (!type.IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            var values = GetValues<T>();

            var result = new Dictionary<string, int>();

            int i = 0;
            foreach (var value in values)
                result.Add((value as Enum).GetStringValue(), i++);

            return result;

        }

        public static void AddRange<T>(this ICollection<T> c, IEnumerable<T> range)
        {
            foreach (var i in range)
                c.Add(i);
        }

        public static string RenderViewToString(string controllerName, string viewName, object model)
        {
            using (var writer = new StringWriter())
            {
                var routeData = new RouteData();
                routeData.Values.Add("controller", controllerName);
                var fakeControllerContext = new ControllerContext(new HttpContextWrapper(new HttpContext(new HttpRequest(null, "http://google.com", null), new HttpResponse(null))), routeData, new FakeController());
                var razorViewEngine = new RazorViewEngine();
                var razorViewResult = razorViewEngine.FindView(fakeControllerContext, viewName, "", false);

                var viewContext = new ViewContext(fakeControllerContext, razorViewResult.View, new ViewDataDictionary(model), new TempDataDictionary(), writer);
                razorViewResult.View.Render(viewContext, writer);
                return writer.ToString();
            }
        }

    }


    public class FakeController : ControllerBase { protected override void ExecuteCore() { } }

}