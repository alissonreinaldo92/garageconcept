﻿using GarageConcept;
using GarageConcept.Models;
using GarageConcept.ViewModels;
using System.Threading.Tasks;

namespace Microsoft.AspNet.Identity
{
    public static class IdentityExtensions
    {

        public static async Task<User> FindByNameOrEmailAsync
            (this ApplicationUserManager userManager, string userNameOrEmail)
        {
            if (userNameOrEmail.Contains("@"))
                return await userManager.FindByEmailAsync(userNameOrEmail) ?? await userManager.FindByNameAsync(userNameOrEmail);
            return await userManager.FindByNameAsync(userNameOrEmail);
        }

    }
}