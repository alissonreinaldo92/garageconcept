﻿using System;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;
using GarageConcept.Models;
using GarageConcept.Services;

namespace GarageConcept.Helpers
{
    public static class AuthHelper
    {

        public static string GetLoggedUserId()
        {
            return HttpContext.Current.User.Identity.GetUserId();
        }

        public static User GetLoggedAppUser()
        {
            if (HttpContext.Current.User == null)
                return null;
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return null;
            UserManager<User> manager = GetUserManager();
            return manager.FindById(GetLoggedUserId());
        }

        public static async Task<User> GetLoggedAppUserAsync()
        {
            if (HttpContext.Current.User == null)
                return null;
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return null;
            UserManager<User> manager = GetUserManager();
            return await manager.FindByIdAsync(HttpContext.Current.User.Identity.GetUserId());
        }

        public static MailAddress GetEmailSenderFromConfig()
        {
            return new MailAddress(ConfigurationManager.AppSettings["EmailSenderAddress"].ToString(), ConfigurationManager.AppSettings["EmailSenderName"].ToString());
        }

        //public static async Task<string> GetLoggedUserEmail(User loggedAppUser = null)
        //{
        //    loggedAppUser = loggedAppUser ?? await GetLoggedAppUserAsync();
        //    if (loggedAppUser == null)
        //        return null;
        //    return loggedAppUser.Email;
        //}

        public static UserManager<User> GetUserManager()
        {
            return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
        }

        public static bool IsAdmin()
        {
            try
            {
                return (IsAuthenticated() && RoleAdminService.HasRole(HttpContext.Current.User.Identity.GetUserId(), Roles.SiteAdmin.GetStringValue()));
            }
            catch (InvalidOperationException)
            {
                SignOut();
                return false;
            }
        }

        public static void SignOut()
        {
            HttpContext.Current.GetOwinContext().Authentication.SignOut();
            HttpContext.Current.GetOwinContext().Authentication.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);
        }

        public static bool IsAuthenticated()
        {
            if (HttpContext.Current.User == null || HttpContext.Current.User.Identity == null)
                return false;
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

    }
}