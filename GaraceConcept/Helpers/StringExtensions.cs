﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace System
{
    public static class StringExtensions
    {

        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static string JoinWithLast(this IEnumerable<string> str, string separator, string lastSeparator)
        {
            return String.Concat(String.Join(separator, str.ToArray(), 0, str.Count() - 1), lastSeparator, str.LastOrDefault());
        }

        public static string NormalizeBase64String(this string str)
        {
            if (string.IsNullOrEmpty(str) || !str.Contains("data:"))
                return str;
            int commaIndex = str.IndexOf(',');
            return str.Substring(commaIndex + 1);
        }

        public static bool ContainsIgnoreCaseAndAccent(this string source, string toCheck)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            return compareInfo.IndexOf(source, toCheck, CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreCase) >= 0;
        }

        public static string Limit(this string s, int startIndex, int length)
        {
            if (s.Length > length)
                s = s.Substring(startIndex, length);
            return s;
        }
    }
}